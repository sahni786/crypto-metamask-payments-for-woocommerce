<?php
/**
 * Plugin Name:Crypto Payments Metamask For Woocommerce
 * Description:Add Binance pay payment gateway for your woocommerce store.
 * Plugin URI:https://coolplugins.net/
 * Author:Cool Plugins
 * Author URI:https://coolplugins.net/
 * Version: 1.0
 * License: GPL2
 * Text Domain: CPMW
 * Domain Path: /languages
 *
 * @package Binance_api_test
 */

/*
Copyright (C) 2018  CoolPlugins contact@coolplugins.net

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

if (!defined('ABSPATH')) {
    exit;
}
define('CPMW_VERSION', '1.0');
define('CPMW_FILE', __FILE__);
define('CPMW_PATH', plugin_dir_path(CPMW_FILE));
define('CPMW_URL', plugin_dir_url(CPMW_FILE));
/*** Cpmw_metamask_pay main class by CoolPlugins.net */
if (!class_exists('Cpmw_metamask_pay')) {
    final class Cpmw_metamask_pay
    {
        /**
         * The unique instance of the plugin.
         *
         */
        private static $instance;

        /**
         * Gets an instance of our plugin.
         *
         */
        public static function get_instance()
        {
            if (null === self::$instance) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Constructor.
         */
        private function __construct()
        {

        }

        // register all hooks
        public function registers()
        {
            /*** Installation and uninstallation hooks */
            register_activation_hook(__FILE__, array('Cpmw_metamask_pay', 'activate'));
            register_deactivation_hook(__FILE__, array('Cpmw_metamask_pay', 'deactivate'));			
			add_action('plugins_loaded', array(self::$instance,'cpmw_load_files'));
            add_filter('woocommerce_payment_gateways', array(self::$instance, 'cpmw_add_gateway_class'));
            add_action('wp_ajax_nopriv_cpmw_payment_verify', 'cpmw_payment_verify');
            add_action('wp_ajax_cpmw_payment_verify', 'cpmw_payment_verify');		
            add_action('admin_enqueue_scripts', array($this, 'cmpw_admin_style'));
            $this->cpmw_installation_date();

          
        }        
        function cpmw_installation_date(){
		 $get_installation_time = strtotime("now");
   	 	  add_option('cpmw_activation_time', $get_installation_time ); 
	    }
        public function cmpw_admin_style($hook)
         {
        
        if($hook=="woocommerce_page_wc-settings"){
        wp_enqueue_script('cpmw-custom', CPMW_URL . 'assets/js/cpmw-admin.js', array('jquery'), CPMW_VERSION, true);
        //Add the Select2 CSS file
    wp_enqueue_style( 'select2-css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', array(), '4.1.0-rc.0');

    //Add the Select2 JavaScript file
    wp_enqueue_script( 'select2-js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', 'jquery', '4.1.0-rc.0');

    
        }
        wp_enqueue_style('cpmw_admin_css', CPMW_URL . 'assets/css/cpmw-admin.css',array(), null, null, 'all');


         }
  
		function cpmw_add_gateway_class($gateways)
		{
			$gateways[] = 'WC_cpmw_Gateway'; // your class name is here
			return $gateways;
		}
        /*** Load required files */
        public function cpmw_load_files()
        {         
            if ( ! class_exists( 'WooCommerce' ) ) {
                add_action( 'admin_notices',array( $this,'cpmw_missing_wc_notice' ));
                return;
                }
            /*** Include helpers functions*/
			
            require_once CPMW_PATH . 'includes/cpmw-woo-payment-gateway.php';     
            require_once CPMW_PATH . 'includes/cpmw-functions.php';     
            require_once CPMW_PATH . 'admin/class.review-notice.php';


        }
        function cpmw_missing_wc_notice()
        {
            $installurl = admin_url() . 'plugin-install.php?tab=plugin-information&plugin=woocommerce';   
                if (file_exists(WP_PLUGIN_DIR . '/woocommerce/woocommerce.php')) {
              //  $installurl = admin_url() . 'update.php?action=install-plugin&plugin=woocommerce&_wpnonce=e69e11f273';
                    echo '<div class="error"><p><strong>' .esc_html__('Crypto Payments Metamask For Woocommerce requires WooCommerce to be active', 'cryptapi') . '</div>';

            }
            else {
               //$installurl = admin_url() . 'update.php?action=install-plugin&plugin=woocommerce&_wpnonce=e69e11f273';
              // echo WP_PLUGIN_DIR . '/woocommerce/woocommerce.php';
                echo '<div class="error"><p><strong>' . sprintf(esc_html__('Crypto Payments Metamask For Woocommerce requires WooCommerce to be installed and active. You can download %s here.', 'cryptapi'), '<button class="cpmw_modal-toggle" > Install </button>') . '</strong></p></div>';
                  ?>
                <div class="cpmw_modal">
                    <div class="cpmw_modal-overlay cpmw_modal-toggle"></div>
                    <div class="cpmw_modal-wrapper cpmw_modal-transition">
                    <div class="cpmw_modal-header">
                        <button class="cpmw_modal-close cpmw_modal-toggle"><span class="dashicons dashicons-dismiss"></span></button>
                        <h2 class="cpmw_modal-heading"><?php _e("Install WooCommerce","cpmw")?></h2>
                    </div>
                    
                    <div class="cpmw_modal-body">
                        <div class="cpmw_modal-content">
                        <iframe  src="<?php echo esc_url($installurl); ?>" width="600" height="400" id="cpmw_custom_cpmw_modal"> </iframe>
                        </div>
                    </div>
                    </div>
                </div>
                <?php
            }
        }

        // set settings on plugin activation
        public static function activate()
        {
            update_option("cpmw-v", CPMW_VERSION);
            update_option("cpmw-type", "FREE");
            update_option("cpmw-installDate", date('Y-m-d h:i:s'));
            update_option("cpmw-already-rated", "no");
        }
        public static function deactivate()
        {
            
            delete_option("cpmw-v");
            delete_option("cpmw-type");
            delete_option("cpmw-installDate");
            delete_option("cpmw-already-rated");

        }


    }

}
/*** Cpmw_metamask_pay main class - END */

/*** THANKS - CoolPlugins.net ) */
$cpmw = Cpmw_metamask_pay::get_instance();
$cpmw->registers();

