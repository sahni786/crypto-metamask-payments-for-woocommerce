<?php

if (!defined('ABSPATH')) {
    exit;
}
function cpmw_payment_verify()
{
    global $woocommerce;
    $order_id = sanitize_text_field($_REQUEST['order_id']);
    $nonce = !empty($_REQUEST['nonce']) ? sanitize_text_field($_REQUEST['nonce']) : "";
    $trasn_id=!empty($_REQUEST['payment_processed']) ? sanitize_text_field($_REQUEST['payment_processed']) : "";
  
    $payment_status_d = !empty($_REQUEST['payment_status']) ? sanitize_text_field($_REQUEST['payment_status']) : "";
    $order_expired = !empty($_REQUEST['rejected_transaction']) ? sanitize_text_field($_REQUEST['rejected_transaction']) : "";
    $order = new WC_Order($order_id);
    if ($order->is_paid() || !wp_verify_nonce($nonce, 'cpmw_metamask_pay')) {
        die("*ok*");
    }

    try {
             if ($order_expired == "true" && $trasn_id=="false") {
           // $order->add_meta_data('Payment_status', "CANCEL");
            $order->add_order_note(__('Order has been canceled due to user rejection ', 'cbpw'));
            //  $order->add_meta_data('TransectionId', $trasn_id);
            $order->update_status('wc-cancelled', __('Order has been canceled due to user rejection ', 'cbpw'));
        }
        if($trasn_id!="false"){
           
                if ($payment_status_d == "default") {
                    $order->add_meta_data('TransectionId', $trasn_id);
                    $transection = __('Payment Received via Metamask Pay - Transaction ID:', 'cpmw') . $trasn_id;               
                    $order->add_order_note($transection);
                    $order->payment_complete($trasn_id);
                    // send email to costumer
                    WC()->mailer()->emails['WC_Email_Customer_Processing_Order']->trigger($order_id);
                    // send email to admin
                    WC()->mailer()->emails['WC_Email_New_Order']->trigger($order_id);

                } else {

                    $transection = __('Payment Received via Metamask Pay - Transaction ID:', 'cpmw') . $trasn_id;

                
                    $order->add_order_note($transection);
                    $order->update_status(apply_filters('cpmw_capture_payment_order_status', $payment_status_d));
                    // send email to costumer
                    WC()->mailer()->emails['WC_Email_Customer_Processing_Order']->trigger($order_id);
                    // send email to admin
                    WC()->mailer()->emails['WC_Email_New_Order']->trigger($order_id);
                }           
        }
       
        $order->save_meta_data();
        $data = [
            'is_paid' => $order->is_paid(),           
            'order_status' => $order->get_status(),
        ];
        echo json_encode($data);
        die();

    } catch (Exception $e) {

    }

    echo json_encode(['status' => 'error', 'error' => 'not a valid order_id']);
    die();
}


   function cpmw_format_number($n)
    {

        if ($n >= 25) {
            return $formatted = number_format($n, 2, '.', ',');
        } else if ($n >= 0.50 && $n < 25) {
            return $formatted = number_format($n, 3, '.', ',');
        } else if ($n >= 0.01 && $n < 0.50) {
            return $formatted = number_format($n, 4, '.', ',');
        } else if ($n >= 0.001 && $n < 0.01) {
            return $formatted = number_format($n, 5, '.', ',');
        } else if ($n >= 0.0001 && $n < 0.001) {
            return $formatted = number_format($n, 6, '.', ',');
        } else {
            return $formatted = number_format($n, 8, '.', ',');
        }
    } 

    //Price conversion API start

 function cpmw_price_conversion($total,$crypto,$type)
    {
        global $woocommerce;

        $currency = get_woocommerce_currency();
        $settings_obj = get_option('woocommerce_cpmw_settings');

        if($type=="cryptocompare"){            
            $api = !empty($settings_obj['crypto_compare_key']) ? $settings_obj['crypto_compare_key'] : "";
            if(empty($api)){
            return "no_key";
            }
            $current_price = cpmw_crypto_compare_api($currency, $crypto);
            $current_price_array=(array)$current_price;
            

            if(isset($current_price_array['Response'])){
                return ;
            }
            
            $in_crypto = !empty(($current_price_array[$crypto]) * $total) ? ($current_price_array[$crypto]) * $total : "";
            return $in_crypto;
        }
        else{        
        $price_list=cpmw_openexchangerates_api();        
        if(empty($price_list)){
             return "no_key" ;
        }
        $price_arryay=(array)$price_list->rates;
        $current_rate=$price_arryay[$currency];
        
        $binance_price=cpmw_binance_price_api(''.$crypto.'USDT');       
        $lastprice=$binance_price->lastPrice;
        $cal=($total/$current_rate)/$lastprice;
        return  cpmw_format_number($cal);
        }
    }

      function cpmw_crypto_compare_api($fiat, $crypto_token)
    {   
        $settings_obj = get_option('woocommerce_cpmw_settings');

        $api = !empty($settings_obj['crypto_compare_key']) ? $settings_obj['crypto_compare_key'] : "";
        
        $transient = get_transient("cpmw_currency" . $crypto_token);
        if (empty($transient) || $transient === "") {
            $response = wp_remote_post('https://min-api.cryptocompare.com/data/price?fsym=' . $fiat . '&tsyms=' . $crypto_token . '&api_key='.$api.'', array('timeout' => 120, 'sslverify' => true));
            if (is_wp_error($response)) {
                $error_message = $response->get_error_message();
                return $error_message;
            }
            $body = wp_remote_retrieve_body($response);
            $data_body = json_decode($body);
            set_transient("cpmw_currency" . $crypto_token, $data_body, 10 * MINUTE_IN_SECONDS);
            return $data_body;
        } else {
            return $transient;
        }
    }

     function cpmw_openexchangerates_api()
    {
        $settings_obj = get_option('woocommerce_cpmw_settings');

        $api = !empty($settings_obj['openexchangerates_key'])?$settings_obj['openexchangerates_key']:"";
        if(empty($api)){
            return;
        }

        $transient = get_transient("cpmw_openexchangerates");
        if (empty($transient) || $transient === "") {
            $response = wp_remote_post('https://openexchangerates.org/api/latest.json?app_id='.$api.'', array('timeout' => 120, 'sslverify' => true));
            if (is_wp_error($response)) {
                $error_message = $response->get_error_message();
                return $error_message;
            }
            $body = wp_remote_retrieve_body($response);
            $data_body = json_decode($body);
            set_transient("cpmw_openexchangerates", $data_body, 120 * MINUTE_IN_SECONDS);
            return $data_body;
        } else {
            return $transient;
        }
    }
     function cpmw_binance_price_api($symbol)
    {
        $trans_name="cpmw_binance_price".$symbol;
        $transient = get_transient($trans_name);
        if (empty($transient) || $transient === "") {
            $response = wp_remote_get('https://api.binance.com/api/v3/ticker/24hr?symbol='.$symbol.'', array('timeout' => 120,'sslverify' => true));
           

            if (is_wp_error($response)) {
                $error_message = $response->get_error_message();
                return $error_message;
            }
            $body = wp_remote_retrieve_body($response);
            $data_body = json_decode($body);
            set_transient($trans_name, $data_body, 10 * MINUTE_IN_SECONDS);
            return $data_body;
        } else {
            return $transient;
        }
    }

//Price conversion API end here


   function cpmw_supported_currency(){
        $oe_currency=array("AED","AFN","ALL","AMD","ANG","AOA","ARS","AUD","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BND","BOB","BRL","BSD","BTC","BTN","BWP","BYN","BZD","CAD","CDF","CHF","CLF","CLP","CNH","CNY","COP","CRC","CUC","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","EUR","FJD","FKP","GBP","GEL","GGP","GHS","GIP","GMD","GNF","GTQ","GYD","HKD","HNL","HRK","HTG","HUF","IDR","ILS","IMP","INR","IQD","IRR","ISK","JEP","JMD","JOD","JPY","KES","KGS","KHR","KMF","KPW","KRW","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MRU","MUR","MVR","MWK","MXN","MYR","MZN","NAD","NGN","NIO","NOK","NPR","NZD","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","RUB","RWF","SAR","SBD","SCR","SDG","SEK","SGD","SHP","SLL","SOS","SRD","SSP","STD","STN","SVC","SYP","SZL","THB","TJS","TMT","TND","TOP","TRY","TTD","TWD","TZS","UAH","UGX","USD","UYU","UZS","VES","VND","VUV","WST","XAF","XAG","XAU","XCD","XDR","XOF","XPD","XPF","XPT","YER","ZAR","ZMW","ZWL",);
        return $oe_currency;
    }

  