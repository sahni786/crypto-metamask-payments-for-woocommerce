<?php
if (!defined('ABSPATH')) {
    exit;
}

class WC_cpmw_Gateway extends WC_Payment_Gateway
{

    /**
     * Class constructor, more about it in Step 3
     */
    public function __construct()
    {

        $this->id = 'cpmw'; // payment gateway plugin ID
        $this->icon = CPMW_URL . '/assets/images/metamask.png'; // URL of the icon that will be displayed on checkout page near your gateway name
        $this->has_fields = true; // in case you need a custom credit card form
        $this->method_title = __('Metamask Pay', 'cpmw');
        $this->method_description = __('Crypto Payments Metamask For Woocommerce', 'cpmw'); // will be displayed on the options page
        // gateways can support subscriptions, refunds, saved payment methods,
        // but in this tutorial we begin with simple payments

        // Method with all the options fields
        $this->init_form_fields();
        // Load the settings.
        $this->init_settings();
        $this->enabled = $this->get_option('enabled');
        $this->title = $this->get_option('title');
        $this->email_enabled = $this->get_option('email_enabled');
        $this->description = $this->get_option('custom_description');
        $this->default_status = apply_filters('cpmw_process_payment_order_status', 'pending');
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
        add_filter('woocommerce_form_field_multiselect', array($this, 'cpmw_custom_multiselect_handler', 10, 4));

        // add_action('admin_enqueue_scripts', array($this, 'payment_scripts'));
        if (!$this->is_valid_for_use()) {
            $this->enabled = 'no';
        }

    }

    

function cpmw_custom_multiselect_handler($field, $key, $args, $value)
{

    $options = '';

    if (!empty($args['options'])) {
        foreach ($args['options'] as $option_key => $option_text) {
            $options .= '<option value="' . $option_key . '" ' . selected($value, $option_key, false) . '>' . $option_text . '</option>';
        }

        if ($args['required']) {
            $args['class'][] = 'validate-required';
            $required = '&nbsp;<abbr class="required" title="' . esc_attr__('required', 'woocommerce') . '">*</abbr>';
        } else {
            $required = '&nbsp;<span class="optional">(' . esc_html__('optional', 'woocommerce') . ')</span>';
        }

        $field = '<p class="form-row ' . implode(' ', $args['class']) . '" id="' . $key . '_field">
            <label for="' . $key . '" class="' . implode(' ', $args['label_class']) . '">' . $args['label'] . $required . '</label>
            <select name="' . $key . '" id="' . $key . '" class="select" multiple="multiple">
                ' . $options . '
            </select>
        </p>' . $args['after'];
    }

    return $field;
}


    public function is_valid_for_use()
    {
        if (in_array(get_woocommerce_currency(), apply_filters('cpmw_supported_currencies', cpmw_supported_currency()))) {
            return true;
        }

        return false;
    }
    /**
     * Custom CSS and JS
     */
    /*  public function payment_scripts()
    {
    // if our payment gateway is disabled, we do not have to enqueue JS too
    if ('no' === $this->enabled) {
    return;
    }

    wp_enqueue_script('cpmw-custom', CPMW_URL . 'assets/js/cpmw-admin.js', array('jquery'), CPMW_VERSION, true);

    } */

    /**
     * Plugin options, we deal with it in Step 3 too
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => 'Enable/Disable',
                'label' => 'Enable Metamask Pay',
                'type' => 'checkbox',
                'description' => '',
                'default' => 'yes',
            ),
            'user_wallet' => array(
                'title' => 'Payment address<span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => '',
            ),
             'user_private_key' => array(
                'title' => 'Metamask Private Key <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => '',
            ),
            'currency_conversion_api' => array(
                'title' => 'Select Currency conversion API',
                'type' => 'select',
                'description' => 'Select currency conversion API for your products',
                'default' => 'openexchangerates',
                'options' => array(
                    'cryptocompare' => __('Cryptocompare', 'cpmw'),
                    'openexchangerates' => __('Binance + Openexchangerates', 'cpmw'),
                ),

            ),
            'crypto_compare_key' => array(
                'title' => 'Enter cryptocompare key <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => 'Get your  key<a href="https://min-api.cryptocompare.com/documentation?key=Price&cat=SingleSymbolPriceEndpoint" target="_blank"> Link</a>',
            ),
            'openexchangerates_key' => array(
                'title' => 'Enter Openexchangerates key <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => 'Get your  key<a href="https://openexchangerates.org/account/app-ids" target="_blank"> Link</a>',
            ),
            /*       'cpmw_public_key' => array(
            'title' => 'Api Key <span style="color:red">(Required *)</span>',
            'type' => 'text',
            'description' => 'Get your API key<a href="https://developers.binance.com/docs/binance-pay/authentication" target="_blank"> Link</a>',
            ),
            'cpmw_private_key' => array(
            'title' => 'Api Secert Key <span style="color:red">(Required *)</span>',
            'type' => 'text',
            'description' => 'Get your API secret key<a href="https://developers.binance.com/docs/binance-pay/authentication" target="_blank"> Link</a>',
            ),

            'expiry_time' => array(
            'title' => 'Select Order Expiry Time',
            'type' => 'select',
            'description' => 'Select Expiry time for your product',
            'default' => '45',
            'options' => array(
            '50' => __('10 Minutes', 'cpmw'),
            '45' => __('15 Minutes', 'cpmw'),
            '40' => __('20 Minutes', 'cpmw'),
            '35' => __('25 Minutes', 'cpmw'),
            '30' => __('30 Minutes', 'cpmw'),
            '25' => __('35 Minutes', 'cpmw'),
            '20' => __('40 Minutes', 'cpmw'),
            ),

            ),
            'cpmw_merchantId' => array(
            'title' => 'Merchant Id <span style="color:red">(Required *)</span>',
            'type' => 'text',
            'description' => 'The merchant account id, issued when merchant been created at Binance',
            'default' => '',

            ),
            'cpmw_sub_merchantId' => array(
            'title' => 'Sub Merchant Id (Optional)',
            'type' => 'text',
            'description' => 'The sub merchant account id, issued when sub merchant been created at Binance',
            'default' => '',

            ), */
           
           
            'Chain_network' => array(
                'title' => 'Select Metamask network',
                'type' => 'select',                
                'default' => '0x1',
                'options' => array(
                '0x1' => __('Ethereum Main Network (Mainnet)', 'cpmw'),
                '0x3' => __('Ropsten Test Network', 'cpmw'),
                '0x4' => __('Rinkeby Test Network', 'cpmw'),
                '0x38' => __('Binance Smart Chain (Mainnet)', 'cpmw'),
                '0x61' => __('Binance Smart Chain (Testnet)', 'cpmw'),                                
                ),
            ),
             'eth_select_currency' => array(
                'title' => 'Select Crypto Currency',
                'type' => 'multiselect',
                'description' => 'Select crypto currency to show for payment',
                'default' => '',
                'options' => array(                    
                    'ETH' => __('Ethereum', 'cpmw'),
                    'USDT' => __('TETHER', 'cpmw'),
                ),
               

            ),
            'bnb_select_currency' => array(
                'title' => 'Select Crypto Currency',
                'type' => 'multiselect',
                'description' => 'Select crypto currency to show for payment',
                'default' => 'BNB',
                'options' => array(
                    'BNB' => __('Binance Coin', 'cpmw'),                      
                    'BUSD' => __('BUSD', 'cpmw'),
                ),

            ),
            

            'payment_status' => array(
                'title' => __('Payment Success Status:', 'cpmw'),
                'type' => 'select',
                'description' => __('Payment action on successful Transaction ID submission.', 'cpmw'),
                'desc_tip' => false,
                'default' => 'default',
                'options' => apply_filters('cpmw_settings_order_statuses', array(
                    'default' => __('Woocommerce Default Status', 'cpmw'),
                    'on-hold' => __('On Hold', 'cpmw'),
                    'processing' => __('Processing', 'cpmw'),
                    'completed' => __('Completed', 'cpmw'),
                )),
            ),
             'title' => array(
                'title' => __('Title:', 'cpmw'),
                'type' => 'text',
                'description' => __('This controls the title for the payment method the customer sees during checkout.', 'cpmw'),
                'default' => 'Metamask Pay',
                'desc_tip' => false,
            ),
            'custom_description' => array(
                'title' => 'Description',
                'type' => 'text',
                'description' => 'Add custom description for checkout payment page',
                'default' => 'Payout using metamask',

            ),
            'payement_msg' => array(
                'title' => 'Payment Success Message ',
                'type' => 'text',
                'description' => 'Add custom message for successfull payment',
                'default' => 'Payment Completed Successfully',

            ),
              'confirm_msg' => array(
                'title' => 'Confirm action messgae',
                'type' => 'text',
                'description' => 'Add custom message for process payment',
                'default' => 'Confirm this action in your wallet',

            ),
              'payement_process_msg' => array(
                'title' => 'Payment Processing',
                'type' => 'text',
                'description' => 'Add custom message for process payment',
                'default' => 'Payment in process',

            ),
            'rejected_message' => array(
                'title' => 'Rejected Transaction Message ',
                'type' => 'text',
                'description' => 'Add custom message for rejected Transaction ',
                'default' => 'Transaction Rejected by user',

            ),

        );

    }

    public function payment_fields()
    {
        wp_enqueue_style('ca-loader-css', CPMW_URL . 'assets/css/cpmw.css');
        $get_network=$this->get_option("Chain_network");
        $crypto_currency = ($get_network=='0x1' || $get_network=='0x3'|| $get_network=='0x4')?$this->get_option("eth_select_currency"):$this->get_option("bnb_select_currency");
       
       
        $type = $this->get_option('currency_conversion_api');
        $total_price = $this->get_order_total();
        
        $metamask = "";
        do_action('woocommerce_cpmw_form_start', $this->id);
        
            ?>
                <div class="form-row form-row-wide">
                    <p><?php echo esc_html($this->description); ?></p>
                    <?php  foreach ($crypto_currency as $key => $value) {    
                        $in_busd = cpmw_price_conversion($total_price, $value, $type);
                        if (!empty($in_busd) && $in_busd != "no_key") {
                        ?>
                    <div class="cpmw-pymentfield">
                    <input id="cpmw_payment_method" type="radio" class="input-radio" name="cpmw_crypto_coin" value="<?php echo !empty($in_busd) ? esc_attr($value) : ""; ?>"/>
                        <img src="<?php echo esc_url(CPMW_URL . '/assets/images/'.$value.'.svg'); ?>"/>
                        <span><?php echo esc_html($value) ?></span>
                    <p class="cpmw_crypto_price"><?php echo esc_html($in_busd . $value) ?></p>
                    </div>
                        <?php 
                        
                    } elseif ($in_busd == "no_key") {
            echo __('<strong>Please enter Your price conversion key</strong>', 'cpmw')

            ?>
              <input id="cpmw_no_key" type="hidden"  name="cpmw_no_key" value="<?php echo esc_attr($in_busd); ?>"/>
            <?php

        } else {
            echo __('<strong>Currency is not supported</strong>', 'cpmw')
            ?>
              <input id="cpmw_currency_not_supported" type="hidden"  name="cpmw_currency_not_supported" value="<?php echo esc_attr($in_busd); ?>"/>
            <?php

        }                   
                    
                    
                    } ?>
               </div>
                <?php

        do_action('woocommerce_cpmw_form_end', $this->id);
    }
    

    public function validate_fields()
    {

        $user_wallet = $this->get_option('user_wallet');
        $private_key = !empty($this->get_option('user_private_key')) ? $this->get_option('user_private_key') : "";

        if (empty($user_wallet)) {
            wc_add_notice(__('<strong>Please enter your Metamask Payment address</strong>', 'cpmw'), 'error');
            return false;

        }
         if (empty($private_key)) {
            wc_add_notice(__('<strong>Please enter your Metamask Private Key</strong>', 'cpmw'), 'error');
            return false;

        }
        if (isset($_POST['cpmw_currency_not_supported'])) {
            wc_add_notice(__('<strong>Currency is not supported</strong>', 'cpmw'), 'error');
            return false;

        }
        if (isset($_POST['cpmw_no_key']) && $_POST['cpmw_no_key'] == "no_key") {
            wc_add_notice(__('<strong>Please enter Your price conversion key</strong>', 'cpmw'), 'error');
            return false;
        }
        if (empty($_POST['cpmw_crypto_coin'])) {
            wc_add_notice(__('<strong>Select a Currency for Process Payment </strong>', 'cpmw'), 'error');
            return false;
        }
        return true;
    }

    public function process_payment($order_id)
    {
        global $woocommerce;

        try {
            $order = new WC_Order($order_id);
            $crypto_currency = !empty($_POST['cpmw_crypto_coin']) ? sanitize_text_field($_POST['cpmw_crypto_coin']) : '';
            
            $total = $order->get_total();
            $type = $this->get_option('currency_conversion_api');
            $in_crypto = cpmw_price_conversion($total, $crypto_currency, $type);
            $user_wallet = $this->get_option('user_wallet');
            $order->add_meta_data('cpmw_in_crypto', $in_crypto);
            $order->add_meta_data('cpmw_currency_symbol', $crypto_currency);
            $order->add_meta_data('cpmw_user_wallet', $user_wallet);
            $order->save_meta_data();
            $order->update_status($this->default_status);
            $woocommerce->cart->empty_cart();
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($order),
            );

        } catch (Exception $e) {
            wc_add_notice(__('Payment error:', 'cpmw') . 'Unknown coin', 'error');
            return null;
        }
        wc_add_notice(__('Payment error:', 'woocommerce') . __('Payment could not be processed, please try again', 'cpmw'), 'error');
        return null;
    }

    public function thankyou_page($order_id)
    {
        $payment_msg = !empty($this->get_option('payement_msg')) ? $this->get_option('payement_msg') : "Payment Completed Successfully";
        $confirm_msg = !empty($this->get_option('confirm_msg')) ? $this->get_option('confirm_msg') : "Confirm this action in your wallet";

        $process_msg = !empty($this->get_option('payement_process_msg')) ? $this->get_option('payement_process_msg') : "Payment in process";
        $rejected_msg = !empty($this->get_option('rejected_message')) ? $this->get_option('rejected_message') : "Transaction Rejected by user";
        $network= !empty($this->get_option('Chain_network')) ? $this->get_option('Chain_network') : "";
        $private_key= !empty($this->get_option('user_private_key')) ? $this->get_option('user_private_key') : "";
        $crypto_currency = ($network == '0x1' || $network == '0x3' || $network == '0x4') ? $this->get_option("eth_select_currency") : $this->get_option("bnb_select_currency");

        $order = new WC_Order($order_id);

        $total = $order->get_total();

        $nonce = wp_create_nonce('cpmw_metamask_pay');
        $user_wallet = $order->get_meta('cpmw_user_wallet');
        $in_crypto = $order->get_meta('cpmw_in_crypto');        
        $currency_symbol = $order->get_meta('cpmw_currency_symbol');

        $payment_status = $order->get_status();
        
        wp_enqueue_script('cpmw-ether', 'https://cdn.ethers.io/lib/ethers-5.2.umd.min.js', array('jquery'), CPMW_VERSION, true);
        wp_enqueue_script('cpmw-web3', CPMW_URL . 'assets/js/web3.min.js', array('jquery'), CPMW_VERSION, true);
        wp_enqueue_script('cpmw_custom', CPMW_URL . 'assets/js/cpmw-custom.js', array('jquery'), CPMW_VERSION, true);
        wp_localize_script('cpmw_custom', "extradata", array('currency_symbol'=>$currency_symbol,'private_key'=>$private_key,'confirm_msg'=>$confirm_msg,'network'=>$network,'is_paid'=>$order->is_paid(),'process_msg'=>$process_msg,'payment_msg'=>$payment_msg,'rejected_msg'=>$rejected_msg,'in_crypto' => $in_crypto, 'recever' => $user_wallet, 'ajax' => home_url('/wp-admin/admin-ajax.php'), 'order_status' => $payment_status, 'id' => $order_id,'nonce' => $nonce, 'payment_status' => $this->get_option('payment_status'),
        ));
        wp_enqueue_style('cpmw_custom_css', CPMW_URL . 'assets/css/cpmw.css', array(), CPMW_VERSION, null, 'all');
        ?>
        <div class="cpmw_loader_wrap">
        
        <div class="cpmw_loader">
            <img src="<?php echo esc_url(CPMW_URL . '/assets/images/metamask.png') ?>" alt="metamask" >
            <h2><?php echo esc_html($confirm_msg)?></h2>
                <div>
                    <div class="lds-css ng-scope">
                        <div class="cpmw-lds-dual-ring">
                            <div></div>
                            <div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <div class="cmpw_meta_connect">
           <div class="wallet-icon" >
               <img src="<?php echo esc_url(CPMW_URL . '/assets/images/metamask.png') ?>" alt="metamask" >
            </div>
           
            <div class="connect-wallet" >
                <div class="cpmw_connect_btn">
                    <button class="c-btn" > <?php  _e('MetaMask Connect','cpmw') ?></button>
                </div>
            </div>
        </div>

       <div class="cmpw_meta_wrapper" >
           <div class="container" >
               <div class="cpmw-pay-wallet-icon" >
               <img src="<?php echo esc_url(CPMW_URL . '/assets/images/metamask.png') ?>" alt="metamask" >
            </div>
               <div class="infos" >
                   <div class="connected-wallet" ><span ><?php  _e('Connected wallet:','cpmw') ?> </span><?php  _e('MetaMask','cpmw') ?></div>
                   <div class="active-chain" ><span ><?php  _e('Active chain:','cpmw') ?> </span><p class="cpmw_active_chain"> </p></div>
                </div>
                <div class="infos" >
                    <div class="connected-account" ><span ><?php  _e('Connected account: ','cpmw') ?></span>
                    <div class="account-address" ></div>
                    </div>
                    <div class="order-price" ><span ><?php  _e('Order price:','cpmw') ?> </span><?php echo esc_html(get_woocommerce_currency_symbol() . $total) ?></div>
                </div>
                <div class="clear" ></div>
                <div class="pay-btn-wrapper" ><button class="c-btn" > <?php  echo __('Pay with ','cpmw').esc_html($in_crypto.$currency_symbol )  ?></button></div>
                
            </div>
        </div>



        <?php

    }

}
