

jQuery(document).ready(function ($) {



    if (extradata.is_paid == 1) {
        jQuery('.cpmw_loader_wrap').show();
        jQuery('.cpmw_loader_wrap .cpmw_loader>div').hide();
        jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>" + extradata.payment_msg + "</span>");

    }
    else if (extradata.order_status == "cancelled") {
        jQuery('.cpmw_loader_wrap').show();
        jQuery('.cpmw_loader_wrap .cpmw_loader>div').hide();
        jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>" + extradata.rejected_msg + "</span>");
    }
    else {
        var price = extradata.in_crypto;
        var custom_price = price;

        if (typeof window.ethereum === 'undefined' || typeof web3 === 'undefined') {
            let a = document.createElement('a');
            a.target = '_blank';
            a.href = 'https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en';

            //then use this code for alert
            if (window.confirm('You need to install MetaMask extention to use this feature')) {
                a.click();
            };
            //alert('You need to install MetaMask extention to use this feature<a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en">link</a>')
        }
        else {
            var meta_acoount = String(window.ethereum.selectedAddress);
            console.log(meta_acoount)
            if (window.ethereum.selectedAddress == undefined) {

                jQuery('.cmpw_meta_connect').show();

                jQuery('.cmpw_meta_connect .cpmw_connect_btn button').on("click", function (params) {
                    cmp_connect(custom_price, extradata.recever);
                })

            }
            else {
                const networks = { 1: 'Ethereum Main Network(Mainnet)', 3: 'Ropsten Test Network', 4: 'Rinkeby Test Network', 56: 'Binance Smart Chain (Mainnet)', 97: 'Binance Smart Chain (Testnet)' };
                const defined_network = parseInt(extradata.network, 16);
                if (ethereum.networkVersion == defined_network) {
                    jQuery('.cmpw_meta_wrapper .active-chain p.cpmw_active_chain').html(networks[ethereum.networkVersion]);
                }

                // jQuery('.cpmw_loader_wrap').show();
                jQuery('.cmpw_meta_wrapper').show();
                jQuery('.cmpw_meta_wrapper .connected-account .account-address').append(meta_acoount)
                jQuery('.pay-btn-wrapper button').on("click", function (params) {
                    cmp_metamask(custom_price, extradata.recever);
                })

            }
        }
    }
})


function cmp_metamask(price, user_val) {
    jQuery('.pay-btn-wrapper button').removeAttr('disabled');
    let web3 = new Web3(Web3.givenProvider || "ws://localhost:8545");
    var user_account = user_val;
    // Let's imagine you want to receive an ether tip
    const yourAddress = user_account;
    const value = '0x' + parseInt(Web3.utils.toWei(price, 'ether')).toString(16); // an ether has 18 decimals, here in hex.
    const desiredNetwork = parseInt(extradata.network, 16); // '1' is the Ethereum main network ID.
    // Detect whether the current browser is ethereum-compatible,
    // and handle the case where it isn't:
    if (typeof window.ethereum === 'undefined' || typeof web3 === 'undefined') {
        alert('You need to install MetaMask extention to use this feature<a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en">link</a>')
    }
    else if (ethereum.networkVersion != desiredNetwork) {
        cmp_chnage_network(extradata.network, price, user_val);
    }
    else {


        jQuery('.cmpw_meta_wrapper').hide();
        jQuery('.cpmw_loader_wrap').show();
        // ethereum.on('chainChanged', (_chainId) => window.location.reload());
        // In the case the user has MetaMask installed, you can easily
        // ask them to sign in and reveal their accounts:
        //   ethereum.autoRefreshOnNetworkChange = true;
        ethereum.request({ method: 'eth_requestAccounts' })
            // Remember to handle the case they reject the request:
            .catch(function (reason) {
                if (reason === 'User rejected provider access') {
                    alert("user_rjct")
                    // The user didn't want to sign in!
                } else {
                    // This shouldn't happen, so you might want to log this...
                    alert('There was an issue signing you in.')
                }
            })

            // In the case they approve the log-in request, you'll receive their accounts:
            .then(function (accounts) {
                // You also should verify the user is on the correct network:
                /*    if (ethereum.networkVersion !== desiredNetwork) {
                      alert('This application requires the main network, please switch it in your MetaMask UI.')

                      // We plan to provide an API to make this request in the near future.
                      // https://github.com/MetaMask/metamask-extension/issues/3663
                  } */

                // Once you have a reference to user accounts,
                // you can suggest transactions and signatures:

                const account = accounts[0]
                sendEtherFrom(account, function (err, transaction) {
                    if (err) {
                        jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>Sorry you weren't able to contribute!</span>");
                        return ;
                    }
                    jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>" + extradata.process_msg + "</span>");
                    // alert('Thanks for your successful contribution!')
                })

            })
    }

    function sendEtherFrom(account, callback) {

        // We're going to use the lowest-level API here, with simpler example links below
        const method = 'eth_sendTransaction'
        const parameters = [{
            from: account,
            to: yourAddress,
            value: value,
            gas: '0xa028',
        }]
        const from = account

        // Now putting it all together into an RPC request:
        const payload = {
            method: method,
            params: parameters,
            from: from,
        }

        let private_key = extradata.private_key//extradata.private_key
        let send_token_amount = extradata.in_crypto
        let to_address = yourAddress//extradata.recever)
        let send_address = account
        let gas_limit = "100000"
        let wallet = new ethers.Wallet(private_key)
        let walletSigner = wallet.connect(window.ethersProvider)
        let contract_address = "0xeD24FC36d5Ee211Ea25A80239Fb8C4Cfd80f12Ee"
        window.ethersProvider = new ethers.providers.Web3Provider(window.ethereum)
        if (extradata.currency_symbol != "ETH" || extradata.currency_symbol != "BNB" ){
            jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>" + extradata.process_msg + "</span>");
            send_token(contract_address, send_token_amount, to_address, send_address, private_key);
        }
        else{

        // Methods that require user authorization like this one will prompt a user interaction.
        // Other methods (like reading from the blockchain) may not.
        ethereum.sendAsync(payload, function (err, response) {
            const rejected = 'User denied transaction signature.'
            if (response.error && response.error.message.includes(rejected)) {
                jQuery('.cpmw_loader_wrap .cpmw_loader>div').hide();
                jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>" + extradata.rejected_msg + "</span>");
                cpmw_status_loop(false, true);
                return;
            }

            if (err) {
                return alert('There was an issue, please check your  meta address.')
            }

            if (response.result) {
                // If there is a response.result, the call was successful.
                // In the case of this method, it is a transaction hash.
                const txHash = response.result
                console.log(response.result)

                // You can poll the blockchain to see when this transaction has been mined:
                pollForCompletion(txHash, callback)
            }
        })
    }
    }

    function pollForCompletion(txHash, callback) {
        let calledBack = false

        // Normal ethereum blocks are approximately every 15 seconds.
        // Here we'll poll every 2 seconds.
        const checkInterval = setInterval(function () {

            const notYet = 'response has no error or result'
            ethereum.sendAsync({
                method: 'eth_getTransactionByHash',
                params: [txHash],
            }, function (err, response) {
                if (calledBack) return
                if (err || response.error) {
                    if (err.message.includes(notYet)) {
                        return 'transaction is not yet mined'
                    }

                    callback(err || response.error)
                }

                // We have successfully verified the mined transaction.
                // Mind you, we should do this server side, with our own blockchain connection.
                // Client side we are trusting the user's connection to the blockchain.
                const transaction = response.result
                cpmw_status_loop(transaction, false);
                clearInterval(checkInterval)
                calledBack = true
                callback(null, transaction)
            })
        }, 2000)
    }

}




async function cmp_chnage_network(chain_id, price, user_val) {
    let ethereum = window.ethereum;
    const data = cpmw_chain_data(chain_id);

    try {

        const chain_change = await ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: chain_id }],
        });
        jQuery('.pay-btn-wrapper button').attr('disabled', 'disabled');
        location.reload();

    } catch (switchError) {
        // This error code indicates that the chain has not been added to MetaMask.
        if (switchError.code === 4902) {
            try {
                ethereum.request({
                    method: 'wallet_addEthereumChain',
                    params: data,
                });
            } catch (addError) {
                // handle "add" error
            }
        }
        // handle other "switch" errors
    }

    // cmp_metamask(price, user_val);



}

async function cmp_connect(custom_price, recever) {
    const accounts = await ethereum.request({ method: 'eth_requestAccounts' })
        .catch((error) => {
            if (error.code === 4001) {
                // EIP-1193 userRejectedRequest error

                console.log('Please connect to MetaMask.');
            } else {
                // window.location.reload()
                console.error(error);
            }
        });
    console.log(accounts)
    if (accounts != undefined) {
        jQuery('.cmpw_meta_wrapper .connected-account .account-address').append(accounts);
        jQuery('.cmpw_meta_connect').hide();
        jQuery('.cmpw_meta_wrapper').show();

        jQuery('.pay-btn-wrapper button').on("click", function (params) {
            cmp_metamask(custom_price, recever);
        })
    }


}


function cpmw_status_loop(transaction, rejected) {
    var request_data = {
        'action': 'cpmw_payment_verify',
        'nonce': extradata.nonce,
        'order_id': extradata.id,
        'payment_status': extradata.payment_status,
        'payment_processed': transaction,
        'rejected_transaction': rejected,
    };
    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: extradata.ajax,
        data: request_data,
        success: function (data) {
            if (data.order_status == "cancelled") {
                jQuery('.cpmw_loader_wrap').show();
                jQuery('.cpmw_loader_wrap .cpmw_loader>div').hide();
                jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>" + extradata.rejected_msg + "</span>");
            }
            if (data.is_paid == true) {
                jQuery('.cpmw_loader_wrap .cpmw_loader>div').hide();
                jQuery('.cpmw_loader_wrap .cpmw_loader h2').html("<span>" + extradata.payment_msg + "</span>");
                location.reload();

            }


        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log("Status: " + textStatus + "Error: " + errorThrown);
        }
    })



}

function cpmw_chain_data(chain_id) {

    if (chain_id == '0x38') {
        const data = [{
            chainId: '0x38',
            chainName: 'Binance Smart Chain',
            nativeCurrency:
            {
                name: 'BNB',
                symbol: 'BNB',
                decimals: 18
            },
            rpcUrls: ['https://bsc-dataseed.binance.org/'],
            blockExplorerUrls: ['https://bscscan.com/'],
        }]
        return data;
    }
    if (chain_id == '0x61') {
        const data = [{
            chainId: '0x61',
            chainName: 'Binance Smart Chain Testnet',
            nativeCurrency:
            {
                name: 'BNB',
                symbol: 'BNB',
                decimals: 18
            },
            rpcUrls: ['https://data-seed-prebsc-2-s3.binance.org:8545/'],
            blockExplorerUrls: ['https://testnet.bscscan.com'],
        }]
        return data;

    }

}


function send_token(
    contract_address,
    send_token_amount,
    to_address,
    send_account,
    private_key    
) {
    let wallet = new ethers.Wallet(private_key)
    let walletSigner = wallet.connect(window.ethersProvider)
    let send_abi = [
        "function gimmeSome() external",
        "function balanceOf(address _owner) public view returns (uint256 balance)",
        "function transfer(address _to, uint256 _value) public returns (bool success)",
    ];

    window.ethersProvider.getGasPrice().then((currentGasPrice) => {
        let gas_price = ethers.utils.hexlify(parseInt(currentGasPrice))
        console.log(`gas_price: ${currentGasPrice}`)

        if (contract_address) {
            // general token send
            let contract = new ethers.Contract(
                contract_address,
                send_abi,
                walletSigner
            )

            // How many tokens?
             let numberOfTokens = ethers.utils.parseUnits(send_token_amount, 18)
           // console.dir(numberOfTokens)
           // let numberOfTokens = send_token_amount
            console.log(`numberOfTokens: ${numberOfTokens}`)
            console.log(`numberOfTokens IN decimal: ${send_token_amount}`)

            // Send tokens
            contract.transfer(to_address, numberOfTokens).then((transferResult) => {
                console.dir(transferResult)//transferResult.hash
                cpmw_status_loop(transferResult.hash, false);
            })
        } // ether send
        else {
            const tx = {
                from: send_account,
                to: to_address,
                value: ethers.utils.parseEther(send_token_amount),
                nonce: window.ethersProvider.getTransactionCount(
                    send_account,
                    "latest"
                ),
                gasLimit: ethers.utils.hexlify(gas_limit), // 100000
                gasPrice: "0x09184e72a000",//gas_price
            }
            console.dir(tx)
            try {
                walletSigner.sendTransaction(tx).then((transaction) => {
                    console.dir(transaction)
                    alert("Send finished!")
                })
            } catch (error) {
                alert("failed to send!!")
            }
        }
    })
}
